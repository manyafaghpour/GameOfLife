# À propos de GameOfLife 
[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com) [![forthebadge](https://forthebadge.com/images/badges/powered-by-electricity.svg)](https://forthebadge.com) 

C'est un automate cellulaire : une grille à deux dimensions dont les cases peuvent prendre deux états distincts : « vivante » ou « morte ». Ces derniers sont régies par trois [règles](https://pastebin.com/esyTU5TD). 

## Pour commencer

```
Bientôt disponible
```

### **Pré-requis**

```
Bientôt disponible
```

### **Installation**

```
Bientôt disponible
```

## Démarrage

```
Bientôt disponible
```

## Frabriqué avec

- [Visual Studio Code](https://code.visualstudio.com/) - Editeur de code
- [Atom](https://atom.io/) - Editeur de textes

## Versions
**Dernière version stable** : x **Dernière version** : x Liste des versions : [Cliquer pour afficher]() (Bientôt disponible)
## Auteurs

- Many Afaghpour alias @manyafaghpour
- Andreas Chatel alias @999-andreas
- Matteo Paolini alias @Shiryu411

## License

Ce projet est sous licence ```WTFTPL``` - voir le fichier [LICENSE.md]() (Bientôt disponible) pour plus d'informations
